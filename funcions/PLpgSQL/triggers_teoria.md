# Triggers

Un *trigger* (disparador) és una acció que s'executarà automàticament quan fem
una alteració a una taula o a una vista.

A les taules, els triggers s'executen **abans** o **després** d'un INSERT,
UPDATE o DELETE. Es pot fer un únic cop per sentència o per cada fila afectada.
En els triggers d'UPDATE es pot concretar que només s'executin si afecten
determinades columnes.

A les vistes, es poden definir triggers per tal de substituir les comandes
INSERT, UPDATE o DELETE. També podem definir triggers per tal que s'executin un
cop abans d'un INSERT, UPDATE o DELETE.

## Creació de triggers

### Sintaxi (simplificada):

```
CREATE TRIGGER name { BEFORE | AFTER | INSTEAD OF } event ON table_name
	[ FOR [ EACH ] { ROW | STATEMENT } ]
	[ WHEN ( condition ) ]
	EXECUTE PROCEDURE function_name ( arguments )
```

A on *event* (esdeveniment) pot ser:

```
	INSERT
	UPDATE [ OF column_name [, ... ] ]
	DELETE
	TRUNCATE
```

### Exemples:


+ Executa la funció `check_account_update()` **abans** de modificar qualsevol fila de
  la taula `accounts`:

	```
	CREATE TRIGGER check_update
		BEFORE UPDATE ON accounts
		FOR EACH ROW
		EXECUTE PROCEDURE check_account_update();
	```

+ El mateix però només si la columna `balance` es modifica:

	```
	CREATE TRIGGER check_update
		BEFORE UPDATE OF balance ON accounts
		FOR EACH ROW
		EXECUTE PROCEDURE check_account_update();
	```

+ El mateix però es comprova que *realment* el valor de la columna `balance` es
  modifica (no només s'intenta modificar, sinó que s'aconsegueix):

	```
	CREATE TRIGGER check_update
		BEFORE UPDATE ON accounts
		FOR EACH ROW
		WHEN (OLD.balance IS DISTINCT FROM NEW.balance)
		EXECUTE PROCEDURE check_account_update();
	```

+ Es crida una funció **després** de l'UPDATE però només si alguna cosa canvia:

	```
	CREATE TRIGGER log_update
		AFTER UPDATE ON accounts
		FOR EACH ROW
		WHEN (OLD.* IS DISTINCT FROM NEW.*)
		EXECUTE PROCEDURE log_account_update();
	```

+ Executa la funció `view_insert_row()` quan volem inserir files a la vista (de
  fet la funció `view_insert_row()` farà INSERT a les taules afectades per la
vista):


	```
	CREATE TRIGGER view_insert
		INSTEAD OF INSERT ON my_view
		FOR EACH ROW
		EXECUTE PROCEDURE view_insert_row();
	```

## Activació i desactivació de triggers

+ Desactivació d'un trigger:

	```
	ALTER TABLE table_name DISABLE TRIGGER trigger_name;
	```

+ Activació d'un trigger:

	```
	ALTER TABLE table_name ENABLE TRIGGER trigger_name;
	```

## Funcions que retornen triggers

Són funcions similars a les que hem fet amb les següents característiques:

+ S'ha de definir abans de crear el trigger. 

+ No ha de tenir paràmetres d'entrada.

+ Ha de retornar el tipus trigger.

+ Les funcions han de retornar el següent:

	- Si el trigger s'executa un sol cop la funció ha de retornar NULL.
	
	- Si el trigger s'executa per cada fila, la funció retorna NULL per no fer
	  l'operació (INSERT/UPDATE/DELETE) per aquella fila. Si es vol fer l'operació, es retornarà la
fila corresponent.

+ Disposem de diverses variables predeterminades que ens ajudaran a codificar
  la funció. Les més importants són:
  
	- NEW: variable de tipus RECORD que conté la nova fila després d'executar
	  una operació INSERT o UPDATE. En canvi serà NULL ... ¹

	- OLD: variable de tipus RECORD que conté la fila antiga abans d'executar
	  una operació UPDATE o DELETE. En canvi serà NULL ... ²

	- TG_*: variables que es refereixen al trigger que ha disparat la funció.
	  [Mirar documentació](https://www.postgresql.org/docs/13/plpgsql-trigger.html#PLPGSQL-DML-TRIGGER).
  
### Exemples


+ Exemple: validació de les dades d'entrada d'una taula 

	```
	CREATE TABLE emp (
		empname text,
		salary integer,
		last_date timestamp,
		last_user text
	);

	CREATE FUNCTION emp_stamp() RETURNS trigger AS $$
	BEGIN
		-- Comprova que empname i salary no siguin nuls
		IF NEW.empname IS NULL THEN
			RAISE EXCEPTION 'empname cannot be null';
		END IF;

		IF NEW.salary IS NULL THEN
			RAISE EXCEPTION '% cannot have null salary', NEW.empname;
		END IF;

		-- Comprovem que no li fem pagar diners per treballar (salari negatiu)
		IF NEW.salary < 0 THEN
			RAISE EXCEPTION '% cannot have a negative salary', NEW.empname;
		END IF;

		-- Afegim la data del canvi de salari i qui ho està canviant ara mateix
		NEW.last_date := current_timestamp;
		NEW.last_user := current_user;
		RETURN NEW;
	END;
	$$ LANGUAGE plpgsql;
	```
	```
	CREATE TRIGGER emp_stamp
		BEFORE INSERT OR UPDATE ON emp
		FOR EACH ROW
		EXECUTE PROCEDURE emp_stamp();
	```

+ Exemple de trigger d'UPDATE d'una vista:

	```
	CREATE VIEW venedor_del_client AS 
		SELECT num_clie, empresa, num_empl, nom 
		  FROM clients 
			   JOIN rep_vendes
			   ON rep_clie = num_empl 
		 ORDER BY num_clie;
	```

	``` 
	CREATE OR REPLACE FUNCTION update_venedor_del_client() RETURNS trigger AS $$
	BEGIN
		-- Si el codi del venedor no exiteix retornarem un error
		IF NEW.num_empl NOT IN (SELECT num_empl
								  FROM rep_vendes) THEN
			RAISE EXCEPTION 'El venedor % no existeix', NEW.num_empl;
		END IF;

		-- Si es modifica el número de client, l'empresa o el venedor del client,
		-- modifiquem la taula clients
		UPDATE clients
		   SET num_clie = NEW.num_clie,
			   empresa = NEW.empresa,
			   rep_clie = NEW.num_empl
		 WHERE num_clie = OLD.num_clie;

		-- Si es modifica el nom del venedor, modifiquem la taula rep_vendes
		-- COMPTE: si no es modifica a NEW tenim el nom antic i es posaria 
		-- aquest nom al venedor nou!
		IF OLD.nom <> NEW.nom THEN
			UPDATE rep_vendes
			   SET nom = NEW.nom
			 WHERE num_empl = NEW.num_empl;
		END IF;
		RETURN NULL;
	END;
	$$ LANGUAGE plpgsql;
	```

	```
	CREATE TRIGGER update_venedor_del_client_trigger
		INSTEAD OF UPDATE ON venedor_del_client
		FOR EACH ROW
		EXECUTE PROCEDURE update_venedor_del_client();
	```



¹ És NULL quan fem servir triggers de sentència, no els que s'executen per
cada fila. I també serà NULL quan l'operació sigui un DELETE.


² És NULL quan fem servir triggers de sentència, no els que s'executen per
cada fila. I també serà NULL quan l'operació sigui un INSERT. 

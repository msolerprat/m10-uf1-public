# PL/pgSQL: Estructura repetitiva

## Bucles simples

### LOOP

És el bucle més senzill. Repeteix les sentències infinitament fins que troba la
sentència `EXIT` o `RETURN`.

####  Sintaxi

```
[ <<label>> ]
LOOP
	statements;
	[ EXIT [ label ] [ WHEN boolean-expression ] | RETURN ... ]; 
END LOOP [ label ];
```

És a dir fixem-nos que es pot sortir a l'arribar a la sentència EXIT de manera
incondicional (no hi ha clàusula WHEN) o bé, en el cas de que existeixi la
clàusula WHEN, si es compleix la condició boleana.


#### Exemples

Funció que rep una cadena i un caràcter i compta quantes vegades conté el
caràcter.

Amb un EXIT sense WHEN:


```
CREATE OR REPLACE FUNCTION compta_caracter(cadena text, caracter char) RETURNS int AS $$
DECLARE
	caracter_actual char;
	num_caracter int := 0;
	i int := 1;
BEGIN
	LOOP
		-- mirem el caràcter a la posició i de la cadena
		caracter_actual := substring(cadena, i, 1);
		-- Si el caràcter actual és el que volem comptar
		-- afegim un al nostre comptador
		IF caracter_actual = caracter THEN
			num_caracter := num_caracter + 1;
		END IF;
		-- incrementem l'índex que marca la posició a la cadena 
		i := i + 1;
		-- Si la posició actual és superior
		-- a la longitud de la cadena, sortim
		IF i > length(cadena) THEN
			EXIT;
		END IF;
	END LOOP;
	-- Només arribarem aquí si passem per l'EXIT
	-- Retornem el comptador de caràcters
	RETURN num_caracter;
END;
$$ LANGUAGE 'plpgsql';
```

La mateixa funció amb un EXIT amb WHEN:

```
CREATE OR REPLACE FUNCTION compta_caracter(cadena text, caracter char) RETURNS int AS $$
DECLARE
	caracter_actual char;
	num_caracter int := 0;
	i int := 1;
BEGIN
	LOOP
		caracter_actual := substring(cadena, i, 1);       
		IF caracter_actual = caracter THEN
			num_caracter := num_caracter + 1;
		END IF;
		i := i + 1;
		EXIT WHEN i > length(cadena);
	END LOOP;
	RETURN num_caracter;
END;
$$ LANGUAGE 'plpgsql';
```

I finalment amb un RETURN 

```
CREATE OR REPLACE FUNCTION compta_caracter(cadena text, caracter char) RETURNS int AS $$
DECLARE
	caracter_actual char;
	num_caracter int := 0;
	i int := 1;
BEGIN
	LOOP
		caracter_actual := substring(cadena, i, 1);
		IF caracter_actual = caracter THEN
			num_caracter := num_caracter + 1;
		END IF;
		i := i + 1;
		IF i > length(cadena) THEN
			RETURN num_caracter;
		END IF;
	END LOOP;
END;
$$ LANGUAGE 'plpgsql';
```

### WHILE

#### Sintaxi


```
[ <<label>> ]
WHILE boolean-expression LOOP
    statements
END LOOP [ label ];
```

#### Exemple

```
CREATE OR REPLACE FUNCTION pow(base numeric, exponent int) RETURNS numeric AS $$
DECLARE
	result numeric := 1;
	i int := 1;
BEGIN
	WHILE i <= exponent LOOP
		result := base * result;
		i := i + 1;
	END LOOP;
	RETURN result;
END;
$$ LANGUAGE 'plpgsql';
```

### FOR ( amb enters)


#### Sintaxi

```
[ <<label>> ]
FOR name IN [ REVERSE ] expression .. expression [ BY expression ] LOOP     -- l'enter name només existeix dintre del loop
    statements
END LOOP [ label ];
```

+ `expression .. expression`fa referència al rang, ñes a dir el valor mínim i
màxim a recòrrer dintre del bucle. 

+ `BY expression` és l'increment o decrement després de cada iteració si s'omet
  val 1.

+ `REVERSE` hi ha decrement en comptes d'increment, és a dir el valor a BY
  expression tindrà signe negatiu. 

#### Exemples

```
CREATE OR REPLACE FUNCTION nums() RETURNS SETOF int AS $$
DECLARE
	i int;
BEGIN
	-- Des de 1 fins a 10, de 1 en 1
	FOR i IN 1..10 LOOP
		-- emmagatzema el valor que hi ha a i
		RETURN NEXT i;
	END LOOP;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';
```

Recordem que quan el retorn d'una funció és un `SETOF` hem de fer servir `RETURN
NEXT` per a cada un dels elements que retornem del SETOF. (O un `RETURN QUERY`).

I aquest RETURN NEXT no farà que sortim de la funció.

En aquest cas concret sortim del bucle FOR quan arribem al valor màxim -> 10

Finalment, s'executa la instrucció `RETURN;` que si que ens treu fora de la funció.
En el cas de no posar-la el sistema l'afegiria, però hi ha raons interessants per posar-la.


Mateixa funció, però ara saltem de 2 en 2:

```
CREATE OR REPLACE FUNCTION nums_2() RETURNS SETOF int AS $$
DECLARE
	i int;
BEGIN
	FOR i IN 1..10 BY 2 LOOP
		RETURN NEXT i;
	END LOOP;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';
```

La mateixa però ara fent decrements:

```
CREATE OR REPLACE FUNCTION nums_desc() RETURNS SETOF int AS $$
DECLARE
	i int;
BEGIN
	FOR i IN REVERSE 10..1 LOOP
		RETURN NEXT i;
	END LOOP;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';
```


## Bucles que recorren resultats de consultes

### FOR 

#### Sintaxi:

```
[ <<label>> ]
FOR target IN query
LOOP
    statements
END LOOP [ label ];
```

A on `target`, que és a on s'emmagatzemmaran els diferents registres de la
query podrà ser: 

+ variable de tipus RECORD.

+ variables de tipus fila. 

+ col·lecció de variables escalars.


#### Exemples

```
CREATE OR REPLACE FUNCTION empl_est() RETURNS SETOF rep_vendes AS $$
DECLARE
	empl RECORD;
BEGIN
	-- Per a cada una de les files que obtenim de la consulta
	-- les retornem (amb RETURN NEXT es van emmagatzemmant)
	FOR empl IN SELECT rep_vendes.*
	              FROM rep_vendes
	                   JOIN oficines
	                   ON oficina_rep = oficina
	                   WHERE regio = 'Est'
	LOOP
		RETURN NEXT empl;
	END LOOP;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';
```

```
SELECT (empl_est()).*;

num_empl |     nom     | edat | oficina_rep |       carrec        | data_contracte | cap |   quota   |  vendes   
----------+-------------+------+-------------+---------------------+----------------+-----+-----------+-----------
      105 | Bill Adams  |   37 |          13 | Representant Vendes | 1988-02-12     | 104 | 350000.00 | 367911.00
      109 | Mary Jones  |   31 |          11 | Representant Vendes | 1989-10-12     | 106 | 300000.00 | 392725.00
      106 | Sam Clark   |   52 |          11 | VP Vendes           | 1988-06-14     |     | 275000.00 | 299912.00
      104 | Bob Smith   |   33 |          12 | Dir Vendes          | 1987-05-19     | 106 | 200000.00 | 142594.00
      101 | Dan Roberts |   45 |          12 | Representant Vendes | 1986-10-20     | 104 | 300000.00 | 305673.00
      103 | Paul Cruz   |   29 |          12 | Representant Vendes | 1987-03-01     | 104 | 275000.00 | 286775.00
(6 rows)
```

Podríem haver escollit, en comptes de:

```
    empl RECORD;
```

aquesta declaració:

```
    empl rep_vendes;
```

o aquesta altra:

```
    empl rep_vendes%ROWTYPE;
```

*D'altra banda per fer aquest exemple no caldria un bucle ( en el sentit
estricte). __Com es podria fer?¹__*


Un altre exemple. Ara volem restar-li al camp vendes de cada venedor l'import
de les seves comandes.


```
CREATE OR REPLACE FUNCTION resta_vendes() RETURNS void AS $$
DECLARE
	rep_import RECORD;
	vendes_venedor_actual numeric;
BEGIN
	-- Recorrem totes les comandes recuperant el venedor que les ha fet i 
	-- l'import de la comanda
	FOR rep_import IN SELECT rep, import
	                    FROM comandes
	LOOP              
		-- Recuperem les vendes del venedor actual
		SELECT vendes
		  INTO vendes_venedor_actual
		  FROM rep_vendes
		 WHERE num_empl = rep_import.rep;
		
		-- Li restem l'import de la comanda
		vendes_venedor_actual := vendes_venedor_actual - rep_import.import;
		
		-- Si en restar-li ens dóna un valor negatiu, el deixem a 0
		IF vendes_venedor_actual < 0 THEN
			vendes_venedor_actual := 0;
		END IF;              
		
		-- Li restem al venedor l'import de la comanda de les seves vendes
		UPDATE rep_vendes
		   SET vendes = vendes_venedor_actual
		 WHERE num_empl = rep_import.rep;          
	END LOOP;
	RETURN;
END;
$$ LANGUAGE 'plpgsql';
```


## Altres

_Les instruccions `CONTINUE`i les etiquetes, reminiscències dels GOTO's de BASIC
són eines que s'haurien d'intentar evitar perquè fan el codi il·legible. Només
en les poques ocasions on la seva absència fa el codi més farragós es podria
fer servir._



¹ La mateix a funció es podria construir amb un RESULT QUERY:


```
CREATE OR REPLACE FUNCTION empl_est() RETURNS SETOF rep_vendes AS $$
DECLARE
    empl RECORD;
BEGIN
	RETURN QUERY SELECT rep_vendes.*
                   FROM rep_vendes
                        JOIN oficines
                        ON oficina_rep = oficina
                        WHERE regio = 'Est';
    RETURN;
END;
$$ LANGUAGE 'plpgsql';
```

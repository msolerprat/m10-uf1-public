# Triggers


## Exercici 1

Es vol fer un control dels canvis produïts a la taula rep_vendes. Per tal de
fer-ho es crearà la taula: canvis_rep_vendes amb les següents columnes:


+ usuari varchar(15): usuari de postgres que ha fet el canvi a la taula
+ data timestamp with time zone: data en la qual s'ha fet la modificaió
+ num_empl_anterior smallint: codi de treballador que hi havia abans del canvi
+ num_empl_nou smallint: codi de treballador que hi ha després del canvi
+ nom_anterior varchar(15): nom del treballador que hi havia abans del canvi
+ nom_nou varchar(15): nom del treballador que hi ha després del canvi

Crea un trigger que insereixi una fila en aquesta taula cada cop que
s'insereixi, es modifiqui o s'esborri un treballador. Si s'insereix un
treballador els camps num_empl_anterior i nom_anterior seran NULL. Si s'esborra
un treballador els camps num_empl_nou i nom_nou seran NULL.

## Exercici 2

Crea un trigger que faci que no es permeti que un treballador pugui ser
director de més de 5 treballadors.

## Exercici 3

Crea un trigger que faci que no es permeti que un treballador pugui ser
director de més d'una oficina.

## Exercici 4

Crea un trigger que faci que no es permeti que es pugi el preu d'un producte
més d'un 20% del preu actual.

## Exercici 5

Crea una taula de nom oficines_baixa amb la següent estructura:
+ oficina smallint
+ ciutat   character varying(15)
+ regio   character varying(10)
+ director      smallint
+ objectiu numeric(9,2)
+ vendes   numeric(9,2)
+ usuari  character varying(15)
+ data    timestamp


Crea un trigger que insereixi una fila a la taula oficines_baixa cada cop que
s'esborri una oficina. Les dades que s'inseriran són les corresponents a
l'oficina que es dóna de baixa excepte les columnes usuari i data on es desaran
l'usuari que dóna de baixa l'oficina i la data actual.


## Exercici 6

Crea un trigger que impedeixi que la quota total dels venedors d'una oficina
(suma de les quotes de tots els seus venedors) sigui inferior a 200000.


## Exercici 7

Crea un trigger que no permeti inserir treballadors a l'oficina de Denver.


## Exercici 8

L'objectiu de l'exercici és fer que no s'eliminin mai les comandes. Per fer
això farem els següents passos:

1. Modifica la taula comandes afegint una columna de nom baixa de tipus
boolean.  Si la comanda s'ha donat de baixa el seu valor serà TRUE, altrament
FALSE.  Feu que quan s'insereixi una comanda aquest camp estigui per defecte a
FALSE.

2. Crea un trigger que faci que quan s'esborri una comanda no l'esborri, però
posi el camp baixa de la comanda a TRUE.

3. No permetis que es modifiqui un registre que s'ha esborrat, a no ser que es
tregui la marca d'esborrat.


## Exercici 9

Fer un trigger que actualitzi el camp estoc de la taula productes cada
cop que s'insereixi o actualitzi una comanda. Es suposa que quan es ven una
quantitat d'un producte, el seu estoc baixa.

## Exercici 10

Crea els triggers necessaris per tal de fer les actualitzacions que consideris
adients a la vista clientes_vip (exercici 4 del llistat d'exercicis de vistes).



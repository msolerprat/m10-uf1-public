# PL/pgSQL: Estructura alternativa

## Condicional IF

### Sintaxi sense ELSE:

```
IF boolean-expression THEN
    statements
END IF;
```

### Sintaxi amb ELSE

```
IF boolean-expression THEN
    statements
ELSE
    statements
END IF;
```

### Sintaxi completa

```
IF boolean-expression THEN
    statements
[ ELSIF boolean-expression THEN
    statements
[ ELSIF boolean-expression THEN
    statements
    ...]]
[ ELSE
    statements ]
END IF;
```

### Exemples


```
CREATE FUNCTION estoc_producte(fab char, prod char) RETURNS int AS $$
DECLARE
	existencies int;
BEGIN
	SELECT estoc
	  INTO existencies
	  FROM productes
	 WHERE id_fabricant = fab
	   AND id_producte = prod;
    
	IF NOT FOUND OR existencies = 0 THEN
		RETURN -1;
	END IF;
    
	RETURN existencies; 
END;
$$ LANGUAGE 'plpgsql';
```


```
CREATE FUNCTION valor_producte(fab char, prod char) RETURNS text AS $$
DECLARE
	total_producte numeric; 
	valor text;
BEGIN
	SELECT estoc * preu
	  INTO total_producte
	  FROM productes
	 WHERE id_fabricant = fab
	   AND id_producte = prod;

	IF total_producte > 50000 THEN
		valor := 'Alt';
	ELSIF total_producte > 10000 THEN
		valor := 'Mig';
	ELSE
		valor := 'Baix';
	END IF;

	RETURN valor; 
END;
$$ LANGUAGE 'plpgsql';
```

Si executem la funció per cada un dels valors de la taula productes:

```
SELECT  id_fabricant, id_producte, valor_producte(id_fabricant, id_producte)
  FROM productes;

 id_fabricant | id_producte | valor_producte 
--------------+-------------+----------------
 rei          | 2a45c       | Mig
 aci          | 4100y       | Alt
 qsa          | xk47        | Mig
 bic          | 41672       | Baix
 imm          | 779c        | Mig
 aci          | 41003       | Mig
 aci          | 41004       | Mig
 bic          | 41003       | Baix
 imm          | 887p        | Baix
 qsa          | xk48        | Mig
 rei          | 2a44l       | Alt
 fea          | 112         | Mig
 imm          | 887h        | Mig
 bic          | 41089       | Mig
 aci          | 41001       | Mig
 imm          | 775c        | Baix
 aci          | 4100z       | Alt
 qsa          | xk48a       | Baix
 aci          | 41002       | Mig
 rei          | 2a44r       | Alt
 imm          | 773c        | Mig
 aci          | 4100x       | Baix
 fea          | 114         | Baix
 imm          | 887x        | Mig
 rei          | 2a44g       | Baix
(25 rows)
```



## Condicional CASE

### Sintaxi simple

```
CASE search-expression
WHEN expression [, expression [ ... ]] THEN
	statements
[ WHEN expression [, expression [ ... ]] THEN
	statements
  	... ]
[ ELSE
	statements ]
END CASE;
```

### Sintaxi examinada o completa (_searched_)

```
CASE
WHEN boolean-expression THEN
	statements
[ WHEN boolean-expression THEN
	statements
	... ]
[ ELSE
	statements ]
END CASE;
```

## Exemples

```
CREATE FUNCTION prima(empleat int) RETURNS numeric AS $$
DECLARE
	years int := 0;
	bonus numeric := 0;
BEGIN
	SELECT DATE_PART('year', age(data_contracte))
	  INTO years
	  FROM rep_vendes
	 WHERE num_empl = empleat; 
	
	CASE years
	WHEN 34 THEN
		bonus := 2500;
	WHEN 35 THEN
		bonus := 5000;
	ELSE
		bonus := 500;
	END CASE;

	RETURN bonus;
END;
$$ LANGUAGE 'plpgsql';
```

Doncs aquesta mena de prima extra que cobrarien els nostres venedors en funció
dels anys que porten treballant, quedaria així:

```
SELECT  num_empl, prima(num_empl) from rep_vendes;
 num_empl | prima 
----------+-------
      105 |  2500
      109 |   500
      102 |  5000
      106 |   500
      104 |  2500
      101 |  5000
      110 |   500
      108 |   500
      103 |  5000
      107 |   500
(10 rows)
```

De la mateixa manera que havíem fet amb el condicional IF

```
CREATE FUNCTION valor_producte(fab char, prod char) RETURNS text AS $$
DECLARE
	total_producte numeric; 
	valor text;
BEGIN
	SELECT estoc * preu
	  INTO total_producte
	  FROM productes
	 WHERE id_fabricant = fab
	   AND id_producte = prod;

	CASE
	WHEN total_producte > 50000 THEN
		valor := 'Alt';
	WHEN total_producte > 10000 THEN
		valor := 'Mig';
	ELSE
		valor := 'Baix';
	END CASE;
	RETURN valor; 
END;
$$ LANGUAGE 'plpgsql';
```
